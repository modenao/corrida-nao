#!/usr/bin/python3
import _thread
from socket import *
import argparse

PORT = 25565
ENCODING = 'utf-8'

MAGIC_BROADCAST_REQUEST = "corrida_discovery".encode(ENCODING)
MAGIC_BROADCAST_RESPONSE = "corrida_response".encode(ENCODING)


def discover_server(port=PORT):
    with socket(AF_INET, SOCK_DGRAM) as s:
        # Setup socket

        s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

        s.sendto(MAGIC_BROADCAST_REQUEST, ('255.255.255.255', port))

        while True:
            data, addr = s.recvfrom(1024)
            if data != MAGIC_BROADCAST_RESPONSE:
                print("Bad response from %s (%s)" % (addr, data.decode(ENCODING)))
                continue
            return addr


def begin_interactive(addr):
    with socket(AF_INET, SOCK_STREAM) as s:
        s.connect(addr)

        def socket_to_console():
            file = s.makefile(encoding=ENCODING)
            while True:
                data = file.readline()
                if not data:
                    break  # Connection closed
                data = data[:-1]  # Remove line break
                print("<'" + str(data) + "'")

        def console_to_socket():
            while True:
                data = input()
                print("Sending " + data)
                if data == "exit":
                    break
                s.send((data + '\n').encode(ENCODING))
            s.shutdown(SHUT_RDWR)
            s.close()

        _thread.start_new_thread(socket_to_console, ())
        console_to_socket()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-ip', type=str, default=None, help='nao ip (or local discovery if nothing is specified)')
    parser.add_argument('-port', type=int, default=PORT, help='corrida port (default 25565)')

    args = parser.parse_args()

    if args.ip:
        addr = (args.ip, args.port)
    else:
        print('Begin server discovery on port %i' % args.port)
        addr = discover_server(args.port)
        print("Server found!")

    begin_interactive(addr)
