<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Corrida" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="choice_sentences_light" src="behavior_1/Aldebaran/choice_sentences_light.xml" />
        <File name="canto_volare" src="canto_volare.mp3" />
        <File name="canto_romagna_mia" src="canto_romagna_mia.mp3" />
        <File name="canto_bella_ciao" src="canto_bella_ciao.mp3" />
        <File name="swiftswords_ext" src="behavior_1/swiftswords_ext.mp3" />
        <File name="dance_macarena" src="dance_macarena.mp3" />
        <File name="dance_quaqua" src="dance_quaqua.mp3" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src="dance_quaqua.mp3" />
    </IgnoredPaths>
</Package>
